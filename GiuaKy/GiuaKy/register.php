<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<form class="form" id="form" onsubmit="return validateForm()">
    <div class="message" id="message">
        <p id="error-message" class="error-message"></p>
    </div>
    <div class="center-form">
        <div class="table">
            <label class="label"> Họ và Tên <span class="validate">*</span> </label>
            <label>
                <input type="textbox" id="ho_ten" class="input" required>
            </label>
        </div>
        <div class="table">
            <label class="label"> Giới tính <span class="validate">*</span> </label>
            <div class="radio">
                <?php
                $genderOptions = array(1 => "Nam", 2 => "Nữ");
                foreach ($genderOptions as $key => $value) {
                    echo '<input required type="radiobox" id="gioi_tinh' . $key . '" name="gioi_tinh" value="' . $key . '" ' . '>';
                    echo '<label for="gioi_tinh_' . $key . '">' . $value . '</label>';
                }
                ?>
            </div>
        <div class="table">
            <label class="label"> Ngày sinh <span class="validate">*</span> </label>
            <label for="namSinh">Năm sinh:</label>
        <select id="namSinh" name="namSinh" required>
        <option value="">-- Chọn năm --</option>
        <?php
        $namHienTai = date("Y");
        $namBatDau = $namHienTai - 20;
        for ($nam = $namBatDau; $nam <= $namHienTai; $nam++) {
            echo "<option value='$nam'>$nam</option>";
        }
        ?>
        </select>
        <label for="ngayThang">Ngày thang:</label>
    <select id="ngayThang" name="ngayThang" required>
        <option value="">-- Chọn tháng --</option>
        <?php
        for ($thang = 1; $thang <= 12; $thang++) {
            echo "<option value='$thang'>$thang</option>";
        }
        ?>
    </select>
         <label for="ngaySinh">Ngày sinh:</label>
    <select id="ngaySinh" name="ngaySinh" required>
        <option value="">-- Chọn ngày --</option>
        <?php
        for ($ngay = 1; $ngay <= 31; $ngay++) {
            echo "<option value='$ngay'>$ngay</option>";
        }
        ?>
    </select>
        </div>
        <label>Địa chỉ:</label>
    <label for="thanhPho">Thành phố:</label>
    <select id="thanhPho" name="thanhPho" required>
        <option value="">-- Chọn thành phố --</option>
        <option value="Hà Nội">Hà Nội</option>
        <option value="Tp.Hồ Chí Minh">Tp.Hồ Chí Minh</option>
    </select>

    <label for="quan">Quận:</label>
    <select id="quan" name="quan" required>
        <option value="">-- Chọn quận --</option>
    </select>

    <input type="submit" value="Đăng ký">
</form>
        <div class="table">
            <label class="label"> Thông tin khác </label>
            <label>
                <input type="text" class="address">
            </label>
        </div>
        <div class="center-btn">
            <input type="submit" id="submit" class="submit-btn" value="Đăng Ký">
        </div>
    </div>
</form>
<script>
    function getElementById(elementId) {
        return document.getElementById(elementId);
    }

    function getRadioValue(radioName) {
        const radios = document.querySelectorAll(`input[name="${radioName}"]`);
        for (const radio of radios) {
            if (radio.checked) {
                return radio.value;
            }
        }
        return null;
    }

    function getSelectValue(selectId) {
        const select = document.getElementById(selectId);
        return select.value;
    }

    function getValueById(elementId) {
        return getElementById(elementId).value;
    }

    const hoTenInput = getElementById('ho_ten');
    const gioiTinhInputs = document.querySelectorAll('input[name="gioi_tinh"]');
    const departmentSelect = getElementById('Department');
    const errorMessageDiv = getElementById('error-message');
    const messageDiv = getElementById('message');
    const form = getElementById('form');

    document.getElementById("submit").addEventListener('click', function () {
        const fullName = getValueById('ho_ten');
        const gender = getRadioValue('gioi_tinh');
        const department = getSelectValue('Department');
        const dateOfBirth = getValueById('ngay_sinh');
        let count = 0;
        let errorMessage = '';
        let errorFlag = false;

        if (fullName === '') {
            errorMessage += 'Hãy nhập tên.<br>';
            errorFlag = true;
            count += 1;
        }

        if (!gender) {
            errorMessage += 'Hãy chọn giới tính.<br>';
            errorFlag = true;
            count += 1;
        }


        if (errorFlag) {
            errorMessageDiv.innerHTML = errorMessage;
        }

        switch (count) {
            case 1:
                messageDiv.style.height = '30px';
                form.style.height = '450px';
                break;
            case 2:
                messageDiv.style.height = '60px';
                form.style.height = '500px';
                break;
            case 3:
                messageDiv.style.height = '90px';
                form.style.height = '530px';
                break;
            case 4:
                messageDiv.style.height = '120px';
                form.style.height = '550px';
                break;
            default:
                break;
        }
    })
    
     var thanhPhoSelect = document.getElementById("thanhPho");
      var quanSelect = document.getElementById("quan");

     var quanOptions = {
        "Hà Nội": ["Quận Hoàng Mai ", "Quận Thanh Trì", "Quận Nam Từ Liêm", "Quận Hà Đông", "Quận Cầu Giấy"],
        "Tp.Hồ Chí Minh": ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"]
    };

    thanhPhoSelect.addEventListener("change", function () {
        // Xóa các lựa chọn quận hiện tại
        quanSelect.innerHTML = "";

        // Lấy danh sách quận tương ứng với thành phố được chọn
        var selectedCity = thanhPhoSelect.value;
        var quanList = quanOptions[selectedCity];

        // Thêm các lựa chọn quận vào trường "Quận"
        for (var i = 0; i < quanList.length; i++) {
            var option = document.createElement("option");
            option.text = quanList[i];
            option.value = quanList[i];
            quanSelect.add(option);
        }
    });
    function validateForm() {
        const selectedDepartment = getSelectValue('Department');
        return selectedDepartment !== '';
    }
</script>
</body>
</html>
