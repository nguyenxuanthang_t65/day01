<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>
<form class="form" id="form" onsubmit="return validateForm()" action="confirm.php" method="post" enctype="multipart/form-data">
    <div class="message" id="message">
        <p id="error-message" class="error-message"></p>
    </div>
    <div class="center-form">
        <div class="table">
            <label class="label"> Họ và Tên <span class="validate">*</span> </label>
            <label>
                <input type="text" id="Full_Name" name="Full_Name" class="input" >
            </label>
        </div>
        <div class="table">
            <label class="label"> Giới tính <span class="validate">*</span> </label>
            <div class="radio">
                <?php
                $Gender_FM = array(0 => "Nam", 1 => "Nữ");
                foreach ($Gender_FM as $key => $value) {
                    echo '<input type="radio" id="Gender_FM"' . $key . '" name="Gender_FM" value="' . $value . '" ' . '>';
                    echo '<label for="Gender_FM_' . $key . '">' . $value . '</label>';
                }
                ?>
            </div>
        </div>
        <div class="table">
            <label class="label"> Phân Khoa <span class="validate">*</span> </label>
            <label>
                <select name="Department" class="Department" id="Department">
                    <option disabled selected hidden value=""> Chọn phân khoa</option>
                    <?php
                    $khoa = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                    foreach ($khoa as $key => $value) {
                        echo '<option value= "' . $value . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </label>
        </div>
        <div class="table">
            <label class="label"> Ngày sinh <span class="validate">*</span> </label>
            <label>
                <input type="date" class="Department" id="Date_Birth" name="Date_Birth">
            </label>
        </div>
        <div class="table">
            <label class="label"> Địa chỉ </label>
            <label>
                <input type="text" class="address" id="address" name="address">
            </label>
        </div>
        <div class="table">
            <label class="label"> Hình ảnh </label>
            <label>
                <input type="file" id="image" class="image" name="image">
            </label>
        </div>
        <div class="center-btn">
            <input type="submit" id="submit" name="submit" class="submit-btn" value="Đăng Ký">
        </div>
    </div>
</form>
<script>
    document.getElementById("submit").addEventListener('click', function () {

        const FName = document.getElementById('Full_Name').value;
        const Gender = document.querySelector('input[name="Gender_FM"]:checked');
        const khoa = document.getElementById('Department').value;
        const DateBirth = document.getElementById('Date_Birth').value;
        const image = document.getElementById('image').value;
        let image_name = image.split('.');
        let extension = image_name.pop();
        let accept_list = ['jpg','png','JPG','PNG'];
        let count = 0;
        let errorMessage = '';
        let errorFlag = false;

        if (FName === '') {
            errorMessage += 'Hãy nhập tên.<br>';
            errorFlag = true;
            count += 1;
        }

        if (!Gender) {
            errorMessage += 'Hãy chọn giới tính.<br>';
            errorFlag = true;
            count += 1;
        }

        if (khoa === '') {
            errorMessage += 'Hãy chọn phân khoa.<br>';
            errorFlag = true;
            count += 1;
        }

        if (DateBirth === '') {
            errorMessage += 'Hãy nhập ngày sinh.<br>';
            errorFlag = true;
            count += 1;
        }

        if (image === '') {
            errorMessage += 'Hãy chọn hình ảnh.<br>';
            errorFlag = true;
            count += 1;
        }else{
            if(!accept_list.includes(extension)){
                errorMessage += 'Định dạng ảnh đang sai.';
                errorFlag = true;
                count += 1;
            }
        }

        if (errorFlag) {
            document.getElementById('error-message').innerHTML = errorMessage;
        }

        switch (count) {
            case 1:
                document.getElementById('message').style.height = '18px';
                document.getElementById('form').style.height = '500px';
                break;
            case 2:
                document.getElementById('message').style.height = '40px';
                document.getElementById('form').style.height = '530px';
                break;
            case 3:
                document.getElementById('message').style.height = '65px';
                document.getElementById('form').style.height = '560px';
                break;
            case 4:
                document.getElementById('message').style.height = '70px';
                document.getElementById('form').style.height = '580px';
                break;
            case 5:
                document.getElementById('message').style.height = '70px';
                document.getElementById('form').style.height = '580px';
                break;
            default:
                break;
        }
    })

    function validateForm() {
        const FName = document.getElementById('Full_Name').value;
        const Gender = document.querySelector('input[name="Gender_FM"]:checked');
        const khoa = document.getElementById('Department').value;
        const DateBirth = document.getElementById('Date_Birth').value;
        const image = document.getElementById('image').value;
        if(FName === ''|| !Gender || khoa === '' || DateBirth === '' || image === ''){
            return false;
        }
    }
</script>
</body>
</html>