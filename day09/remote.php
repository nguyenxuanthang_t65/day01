<?php
include("./config.php");

class remote extends database{
    public function __construct(){
        parent::__construct();
    }
    public function read_user_by_id($id){
        $sql = "SELECT * FROM students WHERE students.ID = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($id));
    }
    public function modify_data($ID ,$username, $gender, $department, $date, $address, $anh){
        include_once("./process_image.php");
        if (isset($_FILES['image'])){
            if (!empty($_FILES['image']['name'])){
                $anh = process_image();
            }
        } 

        $sql = "UPDATE students 
                SET students.HoTen = ? , students.Khoa = ?, students.GioiTinh = ?, students.NgaySinh = ?, students.DiaChi = ?, students.Anh = ?
                WHERE students.ID = ?";
        $this->setQuery($sql);
        $result = $this->execute(array($username, $department, $gender, $date, $address, $anh, $ID));

        if ($result){
            header("location: index.php");
        } else {
            header("location: modify_record.php?id=$ID");
        }

    }

    public function delete_record_by_id($id){

        $sql = "DELETE FROM students WHERE students.ID = ?";
        $this->setQuery($sql);
        $result = $this->execute(array($id));
        if ($result) {
            header("location: index.php");
        }
    }

}
?>