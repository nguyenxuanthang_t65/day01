CREATE DATABASE IF NOT EXISTS ltweb;

CREATE TABLE students (
  ID int(15) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  HoTen varchar(30) NOT NULL,
  Khoa varchar(50) NOT NULL,
  GioiTinh char(1) NOT NULL,
  NgaySinh DATETIME Not NULL,
  DiaChi varchar(50) NOT NULL,
  Anh varchar(50) NOT NULL
);