<?php 
class database{
    public $pdo = NULL;
    public $sql = '';
    public $sta = NULL;

    public $servername;
    public $username;
    public $password;
    public $dbname;



    public function __construct() {
        $this->servername = "localhost";
        $this->username = "root";
        $this->password = "";        
        $this->dbname = "ltweb";
        try
        {
            $this->pdo = new PDO("mysql:host=".$this->servername."; dbname=".$this->dbname,$this->username,$this->password);
            $this->pdo->query('set names "utf8"');
        }
        catch(PDOException $ex )
        {
            die($ex->getMessage());
        }
    }

    public function setQuery($sql) {
        $this->sql = $sql;
    }

    //Function execute the query
    // hàm này sẽ làm hàm chạy câu truy vấn
    public function execute($options=array()) {
        $this->sta = $this->pdo->prepare($this->sql);
        if($options) {  //If have $options then system will be tranmission parameters
            for($i=0;$i<count($options);$i++) {
                $this->sta->bindParam($i+1,$options[$i]);
            }
        }
        $this->sta->execute();
        return $this->sta;
    }

    //Funtion load datas on table
    // lấy nhiều dữ liệu ở trong bảng
    public function loadAllRows($options=array()) {
        if(!$options) {
            if(!$result = $this->execute())
                return false;
        }
        else {
            if(!$result = $this->execute($options))
                return false;
        }
        return $result->fetchAll(PDO::FETCH_OBJ);
    }

    //Funtion load 1 data on the table
    //lay 1 du lieu thoi
    public function loadRow($option=array()) {
        if(!$option) {
            if(!$result = $this->execute())
                return false;
        }
        else {
            if(!$result = $this->execute($option))
                return false;
        }
        return $result->fetch(PDO::FETCH_OBJ);
    }

    //Function count the record on the table
    public function loadRecord($option=array()) {
        if(!$option) {
            if(!$result = $this->execute())
                return false;
        }
        else {
            if(!$result = $this->execute($option))
                return false;
        }
        return $result->fetch(PDO::FETCH_COLUMN);
    }

    public function getLastId() {
        return $this->pdo->lastInsertId();
    }

    public function disconnect() {
        $this->sta=NULL;
        $this->pdo = NULL;
    }
}

?>