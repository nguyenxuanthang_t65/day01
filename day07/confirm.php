<!DOCTYPE html>
<html lang="en">
<head>
    <title>Xác nhận thông tin đăng ký</title>
    <link rel="stylesheet" href="index.css" type="text/css">
</head>
<body>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $target_dir = "upload/";
    $target_file = $target_dir .basename($_FILES["image"]["name"]);
    move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
}
?>
<form class="submit-form" id="submit_form">
    <div class="center-form">
        <div class="check">
            <label class="label"> Họ và Tên <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo "" . $_POST["Full_Name"] . "<br>";
                ?>
            </label>
            <input type = "hidden" name = "username" value=" <?php echo $_POST['Full_Name']; ?>" id="username">
        </div>
        <div class="check">
            <label class="label"> Giới tính <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo "" . $_POST["Gender_FM"] . "<br>";
                ?>
            </label>
            <input type = "hidden" name = "gender" value=" <?php echo  $_POST['Gender_FM']; ?>" id="gender">
        </div>
        <div class="check">
            <label class="label"> Phân Khoa <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo "" . $_POST["Department"] . "<br>";
                ?>
            </label>
            <input type = "hidden" name = "khoa" value=" <?php echo $_POST['Department']; ?>" id="khoa">
        </div>
        <div class="check">
            <label class="label"> Ngày sinh <span class="validate">*</span> </label>
            <label class="font">
                <?php
                $timestamp = strtotime(str_replace('/', '-', $_POST["Date_Birth"]));
                $validate_ngaysinh = date('d/m/Y', $timestamp);
                echo "" . $validate_ngaysinh . "<br>";
                ?>
            </label>
            <input type = "hidden" name = "date" value=" <?php echo $timestamp; ?>" id="date">
        </div>
        <div class="check">
            <label class="label"> Địa chỉ <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo "" . $_POST["address"] . "<br>";
                ?>
            </label>
            <input type = "hidden" name = "address" value=" <?php echo $_POST['address']; ?>" id="address">
        </div>
        <div class="check">
            <label class="label"> Hình ảnh <span class="validate">*</span> </label>
            <label class="font">
                <img src='<?php echo "upload/".basename($_FILES["image"]["name"]); ?>' width="50" height="50" alt="loi hình ảnh">
            </label>
            <input type="hidden" name="avatar" value="<?php echo basename($_FILES["image"]["name"]); ?>" id="avatar">
        </div>
        <div class="center-btn">
            <input type="submit" id="submit" class="submit-btn" value="Xác nhận">
        </div>
    </div>
</form>
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function(){
        $("#submit_form").on('submit', function(e){
            e.preventDefault();
            submitForm();
        })
        function submitForm() {

            var formData = {
                username: $('#username').val(),
                gender: $('#gender').val(),
                khoa: $('#khoa').val(),
                date: $('#date').val(),
                address: $('#address').val(),
                avatar: $('#avatar').val()
            };
            $.ajax({
                type: 'POST',
                url: 'database.php',
                data: formData,
                success: function(response) {
                    console.log(response);
                    alert("insert into database successfully!")
                    window.location = "register.php"
                }
            });
        }
    })
        
    </script>
</html>
