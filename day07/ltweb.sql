SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `student` (
  `Id` int(100) NOT NULL,
  `FullName` varchar(30) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `khoa` varchar(30) NOT NULL,
  `DateofBirth` date NOT NULL,
  `DiaChi` varchar(100) NOT NULL,
  `HinhAnh` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
COMMIT;
