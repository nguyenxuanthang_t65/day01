<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Trang Đăng Nhập</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
            .container {
                margin-top: 100px;
            }
    
            .custom-box {
                background-color: #3498db;
                color: white;
                padding: 10px;
                border-radius: 5px;
            }
        </style>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Đăng Nhập
                    </div>
                    <div style="text-align:center;">
                        <?php
                            $currentTime = new DateTime('now', new DateTimeZone('Asia/Ho_Chi_Minh'));
                            $dayOfWeek = $currentTime->format('N');
                            $weekdays = ["thứ 2","thứ 3","thứ 4","thứ 5","thứ 6","thứ 7","Chủ Nhật"];
                            $dayOfMonth = $currentTime->format('d');
                            $month = $currentTime->format('m');
                            $year = $currentTime->format('Y');
                            $time = $currentTime->format('H:i');
                            $dayName = $weekdays[$dayOfWeek - 1];
                            echo "Bây giờ là $time, $dayName, ngày $dayOfMonth/$month/$year";
                        ?>
                    </div>
                    <div class="card-body">
                        <form method="post" action="process_login.php">
                            <div class="form-group row">
                                <label for="username" class="custom-box">Tên đăng nhập:</label>
                                <div class="col-sm-8">
                                    <input type="text" id="username" name="username" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="custom-box">Mật khẩu:</label>
                                <div class="col-sm-8">
                                    <input type="password" id="password" name="password" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-primary btn-block">Đăng Nhập</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>